#ifndef OSCILLATOR_H
#define OSCILLATOR_H

#pragma once

#include "ofxHarmonograph.h"

class Oscillator
{
 public:
  Oscillator(float freq_in, float phase_in, float ampli_in, float damp_in){
    freq = freq_in;
    phase = phase_in;
    ampli = ampli_in;
    damp = damp_in;
  };
  virtual ~Oscillator(){};
  float dampSin(float t){
    //return int(ampli * ((t * freq + phase)))%120 - 60;// glitch exp
    return ampli * (sin(t * freq + phase)) * exp(-damp * t);   
  };

  void set(float freq_in, float phase_in, float ampli_in, float damp_in){
    freq = freq_in;
    phase = phase_in;
    ampli = ampli_in;
    damp = damp_in;
  }

 protected:
  float freq;
  float phase;
  float ampli;
  float damp;
};

/* float Oscillator::dampSin(float t) { */
 
/* } */

#endif
