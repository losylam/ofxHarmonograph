#include "ofxHarmonograph.h"

Harmonograph::Harmonograph(){
  mesh.setMode(OF_PRIMITIVE_LINE_STRIP);

  DT = 0.01;

  radius = 25;
  n_radius = 4;

  for (int i = 0; i < 2; i++){
    osc_x.push_back(Oscillator(i*2.01, 0, 400, 0.01));
    osc_y.push_back(Oscillator(i, 0, 500, 0.01));
    osc_z.push_back(Oscillator(0, 0, 0, 0));
  }


}

void Harmonograph::setNumPoints(int n_points_in){
  n_points = n_points_in;
  float t = 0;
  mesh.clear();
  for (int i = 0; i< n_points; i++){
    mesh.addVertex(step(t));
    mesh.addIndex(i);
    mesh.addColor(ofColor(255, ofMap(i, 0, n_points, 255, 0)));
    t += DT;
  }
}

void Harmonograph::update(){
  float t = 0;
  for (int i = 0; i< n_points; i++){
    mesh.setVertex(i, step(t));
    //mesh.setColor(i, ofColor(255, ofMap(i, 0, n_points, 255, 0)));
    t += DT;
  }
}

void Harmonograph::draw(){
  mesh.draw();
}

void Harmonograph::draw(int pos_x, int pos_y){
  ofPushMatrix();
  ofTranslate(pos_x, pos_y);
  mesh.draw();
  ofPopMatrix();
}

void Harmonograph::draw3D(int pos_x, int pos_y){
  ofPushMatrix();
  ofTranslate(pos_x, pos_y);
  mesh3d.drawWireframe();
  ofPopMatrix();
}

//--------------------------------------------------------------
ofPoint Harmonograph::step(float t) {
  ofPoint pos = ofPoint(0,0,0);
  for (int i = 0; i < osc_x.size();i++){
    pos.x += osc_x[i].dampSin(t);
  }
  
  for (int i = 0; i < osc_y.size();i++){
    pos.y += osc_y[i].dampSin(t);
  }
  
  for (int i = 0; i < osc_z.size();i++){
    pos.z += osc_z[i].dampSin(t);
  }

  return pos;
}

void Harmonograph::setOsc(int chan, int num, float freq, float phase, float ampli, float damp){
  switch(chan){
  case 0:
    osc_x[num].set(freq, phase, ampli, damp);
    break;
  case 1:
    osc_y[num].set(freq, phase, ampli, damp);
    break;  
  case 2:
    osc_z[num].set(freq, phase, ampli, damp);
    break;
  }
}

void Harmonograph::createRibbon(){
  mesh3d.clear();
  mesh3d.setMode(OF_PRIMITIVE_LINES);
  for (int i = 1; i < mesh.getNumVertices()-1; i++){
    ofPoint cur_pt = mesh.getVertex(i);
    ofPoint next_pt = mesh.getVertex(i+1);
    ofPoint last_pt = mesh.getVertex(i-1);
    ofVec3f dir = next_pt - cur_pt;
    ofVec3f last_dir = cur_pt - last_pt;
    ofVec3f curv = dir.getCrossed(last_dir).getNormalized();
    curv = curv * radius;
    mesh3d.addVertex(cur_pt - curv);
    mesh3d.addColor(ofColor(255, ofMap(i, 0, n_points, 255, 0)));
    
    mesh3d.addVertex(cur_pt + curv);
    mesh3d.addColor(ofColor(255, ofMap(i, 0, n_points, 255, 0)));
  }
}

void Harmonograph::createTube(){
  mesh3d.clear();
  mesh3d.setMode(OF_PRIMITIVE_TRIANGLES);
  for (int i = 1; i < mesh.getNumVertices()-1; i++){
    ofPoint cur_pt = mesh.getVertex(i);
    ofPoint next_pt = mesh.getVertex(i+1);
    ofPoint last_pt = mesh.getVertex(i-1);
    ofVec3f dir = next_pt - cur_pt;
    ofVec3f last_dir = cur_pt - last_pt;
    ofVec3f curv = dir.getCrossed(last_dir).getNormalized();
    curv = curv * radius;
    for (int j = 0;j< n_radius; j++){
      ofVec3f pt_rad = curv.rotate(360/n_radius, dir);
      mesh3d.addVertex(cur_pt + pt_rad);
      mesh3d.addColor(ofColor(255, ofMap(i*n_radius+j, 0, (n_points-2)*n_radius, 255, 0)));
    }
    
  } 

  for (int y = 0; y < n_points - 3; y ++){
    for (int x = 0; x < n_radius -1; x ++){
      mesh3d.addIndex(x+y*(n_radius));
      mesh3d.addIndex((x+1)+y*(n_radius));
      mesh3d.addIndex(x+(y+1)*(n_radius));
      
      mesh3d.addIndex((x+1)+y*(n_radius));           // 1
      mesh3d.addIndex((x+1)+(y+1)*(n_radius));       // 11
      mesh3d.addIndex(x+(y+1)*(n_radius)); 
    }
    
    mesh3d.addIndex(n_radius-1 + y * (n_radius));
    mesh3d.addIndex(y * (n_radius));
    mesh3d.addIndex(n_radius-1 + (y+1) * (n_radius));
    
    mesh3d.addIndex(y * (n_radius));
    mesh3d.addIndex((y+1) * (n_radius));
    mesh3d.addIndex(n_radius-1 + (y+1) * (n_radius));
    
  }
}

void Harmonograph::save(string filename){
  mesh3d.save(filename);
}

Harmonograph::~Harmonograph(){

}
