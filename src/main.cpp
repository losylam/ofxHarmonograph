#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){
  ofSetupOpenGL(1200,900,OF_FULLSCREEN);			// <-------- setup the GL context
  ofSetWindowTitle("cycloid");
  //ofGLESWindowSettings settings;
  //settings.glesVersion = 2;
  //ofCreateWindow(settings);
  // this kicks off the running of my app
  // can be OF_WINDOW or OF_FULLSCREEN
  // pass in width and height too:
  ofRunApp(new ofApp());

}
