#pragma once

#include "ofMain.h"

#include "ofxGui.h"
#include "ofxOscParameterSync.h"
#include "ofxEasing.h"

#include "ofxHarmonograph.h"

class ofApp : public ofBaseApp{

 public:
  void setup();
  void update();
  void draw();

  void keyPressed(int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void mouseEntered(int x, int y);
  void mouseExited(int x, int y);
  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);
  
  float dampSin(float t, float ampli, float freq, float phase, float damp);
  void step();
  float mapX(float x);
  float mapY(float y);  

  // screen
  int width;
  int height;

  int width_max;
  int height_max;

  // fbo

  ofFbo fbo;

  ofTexture fbo_tex;
  ofPixels fbo_pix;
  ofShader shader;

  // GUI
  ofParameterGroup parameters;
  
  ofxOscParameterSync sync;
  
  ofxPanel gui;
  
  ofParameter<bool> mode_slow;
  ofParameter<bool> draw_gui;
  ofParameter<bool> b_record;
  ofParameter<int> n_points;

  ofParameter<float> decrease;

  ofParameter<int> fps;

  ofxPanel gui_presets;
  ofxIntSlider preset;

  // light
  
  ofLight pointLight;
  ofLight pointLight2;
  ofLight pointLight3;
  ofMaterial material;

  
  // HARMONOGRAPH
  // constants

  float MAX_FREQ;
  int PHASE_DIV;
  float MIN_DAMP;
  float MAX_DAMP;
  float DT;
  float FREQ_PERTURBATION;

  ofParameter<float> ampliX1, ampliX2, ampliY1, ampliY2, ampliX3, ampliY3;
  ofParameter<int> animX1, animX2, animY1, animY2, animX3, animY3;
  ofParameter<int> freqX1, freqX2, freqY1, freqY2, freqX3, freqY3;//, freqY3;
  ofParameter<float> freqX1_f, freqX2_f, freqY1_f, freqY2_f, freqX3_f, freqY3_f;//_f, freqY3;
  ofParameter<float> mulY1, mulX2, mulY2;
  ofParameter<float> phaseX1, phaseX2, phaseY1, phaseY2, phaseX3, phaseY3;
  ofParameter<float> dampX1, dampX2, dampY1, dampY2, dampX3, dampY3;
  //  ofParameter<bool> animX1, animX2, animY1, animY2, animX3, animY3;

  float x, y, z, t;

  ofMesh mesh;

  Harmonograph harmo;

  std::vector<Harmonograph> harmos;
  int n_harmo;
  // Audio


  ofParameter<bool> mode_audio;
  ofParameter<bool> draw_audio;

  
  ofEasyCam cam; // add mouse controls for camera movement
};
