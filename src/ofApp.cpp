#include "ofApp.h"

int cpt;
int harmo_w;

int n_harmo_x;
int n_harmo_y;


//--------------------------------------------------------------
void ofApp::setup(){
  ofEnableSmoothing();
  ofSetLineWidth(1.5);

  ofSetSmoothLighting(true);
  
  pointLight.setDiffuseColor( ofFloatColor(.85, .85, .55) );
  //  pointLight.setSpecularColor( ofFloatColor(.8f, .8f, .9f));
  pointLight.setAmbientColor( ofFloatColor(.85, .85, .55) );
  
  pointLight2.setAmbientColor( ofFloatColor(.85, .85, .55) );
  pointLight3.setAmbientColor( ofFloatColor(.85, .85, .55) );
  pointLight2.setDiffuseColor( ofFloatColor( 238.f/255.f, 57.f/255.f, 135.f/255.f ));
  pointLight3.setDiffuseColor( ofFloatColor( 238.f/255.f, 57.f/255.f, 135.f/255.f ));
  pointLight2.setSpecularColor(ofFloatColor(.8f, .8f, .9f));
  
  pointLight.setPosition(500, 500, -100);
  pointLight2.setPosition(500, -500, 100);
  pointLight3.setPosition(500, 0, 0);

  //  material.setShininess(  );
  // the light highlight of the material //
  material.setSpecularColor(ofColor(0, 255, 0));
  

  mesh.setMode(OF_PRIMITIVE_LINE_STRIP);

  //  ofToggleFullscreen();
    
  cam.setDistance(500);
  cam.enableOrtho();

  MAX_FREQ = 10*PI;  
  PHASE_DIV = 12;
  MIN_DAMP = 0.0;
  MAX_DAMP = 0.05;
  DT = 0.01;
  FREQ_PERTURBATION = 0.00;
 
  width_max = 1920;
  height_max = 1080;

  //width = ofGetScreenWidth();
  //height = ofGetScreenHeight();

  width = 900;
  height = 900;
  
  n_harmo = 1;
  harmo_w = width / n_harmo;
  
  n_harmo_x = width/harmo_w;
  n_harmo_y = height/harmo_w;



  // init fbo
  fbo.allocate(width, height, GL_RGB);
  fbo_tex.allocate( width, height, GL_RGB);

  //  shader.load("decrease");

  ofSetCircleResolution(100);
  
  // GUI
  parameters.setName("ofxHarmonograph");

  parameters.add(draw_gui.set("draw gui", true));
  parameters.add(b_record.set("record", false));

  parameters.add(n_points.set("n_points", 10000, 1, 20000));

  parameters.add(ampliX1.set("ampliX1", 1.0, 0, 1.0));
  parameters.add(ampliY1.set("ampliY1", 1.0, 0, 1.0));
  parameters.add(ampliX2.set("ampliX2", 1.0, 0, 1.0));
  parameters.add(ampliY2.set("ampliY2", 1.0, 0, 1.0));
  parameters.add(ampliX3.set("ampliX3", 1.0, 0, 1.0));
  parameters.add(ampliY3.set("ampliY3", 1.0, 0, 1.0));

  parameters.add(animX1.set("animX1", 0, 0, 3));
  parameters.add(animY1.set("animY1", 0, 0, 3));
  parameters.add(animX2.set("animX2", 0, 0, 3));
  parameters.add(animY2.set("animY2", 0, 0, 3));
  parameters.add(animX3.set("animX3", 0, 0, 3));
  parameters.add(animY3.set("animY3", 0, 0, 3));
  
  parameters.add(freqX1.set("freqX1", 1, 0, MAX_FREQ));
  parameters.add(freqY1.set("freqY1", 1, 0, MAX_FREQ));
  parameters.add(freqX2.set("freqX2", 1, 0, MAX_FREQ));
  parameters.add(freqY2.set("freqY2", 1, 0, MAX_FREQ));
  parameters.add(freqX3.set("freqX3", 1, 0, MAX_FREQ));
  parameters.add(freqY3.set("freqY3", 1, 0, MAX_FREQ));

  parameters.add(freqX1_f.set("freqX1_f", 0, 0.0, 0.1));
  parameters.add(freqY1_f.set("freqY1_f", 0, 0.0, 0.1));
  parameters.add(freqX2_f.set("freqX2_f", 0, 0.0, 0.1));
  parameters.add(freqY2_f.set("freqY2_f", 0, 0.0, 0.1));
  parameters.add(freqX3_f.set("freqX3_f", 0, 0.0, 0.1));
  parameters.add(freqY3_f.set("freqY3_f", 0, 0.0, 0.1));
  
  parameters.add(phaseX1.set("phaseX1", 0, 0, 2*PI));
  parameters.add(phaseY1.set("phaseY1", 0, 0, 2*PI));
  parameters.add(phaseX2.set("phaseX2", 0, 0, 2*PI));
  parameters.add(phaseY2.set("phaseY2", 0, 0, 2*PI));
  parameters.add(phaseX3.set("phaseX3", 0, 0, 2*PI));
  parameters.add(phaseY3.set("phaseY3", 0, 0, 2*PI));
  
  parameters.add(dampX1.set("dampX1", 0.01, MIN_DAMP, MAX_DAMP));
  parameters.add(dampY1.set("dampY1", 0.01, MIN_DAMP, MAX_DAMP));
  parameters.add(dampX2.set("dampX2", 0.01, MIN_DAMP, MAX_DAMP));
  parameters.add(dampY2.set("dampY2", 0.01, MIN_DAMP, MAX_DAMP));
  parameters.add(dampX3.set("dampX3", 0.01, MIN_DAMP, MAX_DAMP));
  parameters.add(dampY3.set("dampY3", 0.01, MIN_DAMP, MAX_DAMP));
   
  // parameters.add(mulY1.set("mulY1", 1, 0, MAX_FREQ));
  // parameters.add(mulX2.set("mulX2", 1, 0, MAX_FREQ));
  // parameters.add(mulY2.set("mulY2", 1, 0, MAX_FREQ));

  parameters.add(decrease.set("decrease", 4, 0, 10.0));
  parameters.add(fps.set("fps", 0, 0, 60));
	  
  gui.setup(parameters);

  gui_presets.setup("cycloid_presets"); // most of the time you don't need a name
  gui_presets.add(preset.setup("preset", 0, 0, 20));
  sync.setup((ofParameterGroup&)gui.getParameter(),6669,"localhost",6668);

 
  //setupAudio();
  gui.add(draw_audio.set("draw audio", false));
  gui.add(mode_audio.set("mode audio", false));

  // gui.setPosition(900, 10);
  
  //  gui.loadFromFile("presets/preset_0.xml");  

  harmo.setNumPoints(5000);
  harmo.update();

  for (int i = 0; i< n_harmo*n_harmo;i++){
    Harmonograph new_harmo = Harmonograph();
    new_harmo.setNumPoints(n_points);
    new_harmo.update();
    harmos.push_back(new_harmo);
  }
}

//--------------------------------------------------------------
// void ofApp::setupAudio(){
//   gui.add(draw_audio.set("draw audio", false));
//   gui.add(mode_audio.set("mode audio", true));
  
//   bufferSize    = 512;
//   sampleRate    = 44100;
//   volume        = 0.5f;

//   soundStream.setup(this, 2, 0, sampleRate, bufferSize, 4);

//   lAudio.assign(bufferSize, 0.0);
//   rAudio.assign(bufferSize, 0.0);
// }

//--------------------------------------------------------------
float ofApp::dampSin(float t, float ampli, float freq, float phase, float damp) {
  return ampli * (sin(t * freq + phase)) * exp(-damp * t);
}

//--------------------------------------------------------------
void ofApp::step() {
  x = dampSin(t, ampliX1, freqX1+freqX1_f, phaseX1, dampX1) 
    + dampSin(t, ampliX2, freqX2+freqX2_f, phaseX2, dampX2);
  //      + dampSin(t, ampliX3, freqX3+freqX3_f, phaseX3, dampX3);

  y = dampSin(t, ampliY1, freqY1+freqY1_f, phaseY1, dampY1) 
    + dampSin(t, ampliY2, freqY2+freqY2_f, phaseY2, dampY2);
  //      + dampSin(t, ampliY3, freqY3+freqY3_f, phaseY3, dampY3);

  z =
    dampSin(t, ampliX3, freqX3+freqX3_f, phaseX3, dampX3)
    +    dampSin(t, ampliY3, freqY3+freqY3_f, phaseY3, dampY3);
  

  t += DT;
}

float ofApp::mapX(float x) {
  return ofMap(x, -(ampliX1 + ampliX2 + ampliX3), ampliX1 + ampliX2 + ampliX3, -height/2, height/2);
}

float ofApp::mapY(float y) {
  return ofMap(y, -(ampliY1 + ampliY2 + ampliY3), ampliY1 + ampliY2 + ampliY3, -height/2, height/2);
}


//--------------------------------------------------------------
void ofApp::update(){
  sync.update();
  fps = ofGetFrameRate();

  // freqY1 = mulY1*freqX1;
  // freqX2 = mulX2*freqX1;
  // freqY2 = mulY2*freqY1;
  //correction = ofMap(ofGetLastFrameTime(), 0, 0.02, 0, 1);

  // if (cpt < 20){
  //   //phaseX1 = ofxeasing::map(cpt, 0, 20, 0, PI, ofxeasing::back::easeInOut_s, 0.3);
  //   phaseX1 = ofxeasing::map(cpt, 0, 20, 0, PI, ofxeasing::back::easeInOut_s, 0.3);
  //   //dampY1 = ofxeasing::map(cpt, 0, 20, 0, MAX_DAMP, ofxeasing::back::easeInOut_s, 0.3);
  //   //dampX2 = ofxeasing::map(cpt, 0, 20, 0, MAX_DAMP, ofxeasing::back::easeInOut_s, 0.3);
  // }else if(cpt < 40){
  //   phaseY1 = ofxeasing::map(cpt, 20, 40, 0, PI, ofxeasing::back::easeInOut_s, 0.3);
  //   //phaseX1 = ofxeasing::map(cpt-20, 0, 20, PI/2, PI, ofxeasing::back::easeInOut_s, 0.3);
  //   //dampX1 = ofxeasing::map(cpt-20, 0, 20, 0, MAX_DAMP, ofxeasing::back::easeInOut_s, 0.3);
  //   //dampY2 = ofxeasing::map(cpt-20, 0, 20, 0, MAX_DAMP, ofxeasing::back::easeInOut);
  // }else if(cpt < 60){
  //   //phaseX1 = ofxeasing::map(cpt-40, 0, 20, PI, 2*PI, ofxeasing::back::easeInOut_s, 0.3);
  //   phaseX1 = ofxeasing::map(cpt, 40, 60, PI, 2*PI, ofxeasing::back::easeInOut_s, 0.3);
  //   //dampY1 = ofxeasing::map(cpt-40, 0, 20, MAX_DAMP, 0, ofxeasing::back::easeInOut_s, 0.3);
  //   //dampY2 = ofxeasing::map(cpt-40, 0, 20, MAX_DAMP, 0, ofxeasing::back::easeInOut);
  // }else if(cpt < 80){
  //   //phaseX1 = ofxeasing::map(cpt-60, 0, 20, 3*PI/2, 2*PI, ofxeasing::back::easeInOut_s, 0.3);
  //   phaseY1 = ofxeasing::map(cpt, 60, 80, PI, 2*PI, ofxeasing::back::easeInOut_s, 0.3);
  //   //dampX1 = ofxeasing::map(cpt-60, 0, 20, MAX_DAMP, 0, ofxeasing::back::easeInOut_s, 0.3);
  //   //dampX2 = ofxeasing::map(cpt-60, 0, 20, MAX_DAMP, 0, ofxeasing::back::easeInOut_s, 0.3);
  // }
  if (animX1 == 1){
    phaseX1 = cpt*2*PI/80;
  }
  if (animX2 == 1){
    phaseX2 = cpt*2*PI/80;
  }
  if (animX3 == 1){
    phaseX3 = cpt*2*PI/80;
  }
  if (animY1 == 1){
    phaseY1 = cpt*2*PI/80;
  }
  if (animY2 == 1){
    phaseY2 = cpt*2*PI/80;
  }
  if (animY3 == 1){
    phaseY3 = cpt*2*PI/80;
  }

  if (animX1 == 2){
    if (cpt < 40){
      phaseX1 = ofxeasing::map(cpt, 0, 40, 0, PI, ofxeasing::sine::easeInOut);
    } else {
      phaseX1 = ofxeasing::map(cpt, 40, 80, PI, 2*PI, ofxeasing::sine::easeInOut);
    } 
  }
  if (animX2 == 2){
    if (cpt < 40){
      phaseX2 = ofxeasing::map(cpt, 0, 40, 0, PI, ofxeasing::sine::easeInOut);
    } else {
      phaseX2 = ofxeasing::map(cpt, 40, 80, PI, 2*PI, ofxeasing::sine::easeInOut);
    } 
  }
  if (animX3 == 2){
    if (cpt < 40){
      phaseX3 = ofxeasing::map(cpt, 0, 40, 0, PI, ofxeasing::sine::easeInOut);
    } else {
      phaseX3 = ofxeasing::map(cpt, 40, 80, PI, 2*PI, ofxeasing::sine::easeInOut);
    } 
  }
  if (animY1 == 2){
    if (cpt < 40){
      phaseY1 = ofxeasing::map(cpt, 0, 40, 0, PI, ofxeasing::sine::easeInOut);
    } else {
      phaseY1 = ofxeasing::map(cpt, 40, 80, PI, 2*PI, ofxeasing::sine::easeInOut);
    } 
  }
  if (animY2 == 2){
    if (cpt < 40){
      phaseY2 = ofxeasing::map(cpt, 0, 40, 0, PI, ofxeasing::sine::easeInOut);
    } else {
      phaseY2 = ofxeasing::map(cpt, 40, 80, PI, 2*PI, ofxeasing::sine::easeInOut);
    } 
  }
  if (animY3 == 2){
    if (cpt < 40){
      phaseY3 = ofxeasing::map(cpt, 0, 40, 0, PI, ofxeasing::sine::easeInOut);
    } else {
      phaseY3 = ofxeasing::map(cpt, 40, 80, PI, 2*PI, ofxeasing::sine::easeInOut);
    } 
  }

  if (animX1 == 3){

      phaseX1 = ofxeasing::map(cpt, 0, 80, 0, 2*PI, ofxeasing::sine::easeInOut);

  }
  if (animX2 == 3){

      phaseX2 = ofxeasing::map(cpt, 0, 80, 0, 2*PI, ofxeasing::sine::easeInOut);

  }
  if (animX3 == 3){

      phaseX3 = ofxeasing::map(cpt, 0, 80, 0, 2*PI, ofxeasing::sine::easeInOut);

  }
  if (animY1 == 3){

      phaseY1 = ofxeasing::map(cpt, 0, 80, 0, 2*PI, ofxeasing::sine::easeInOut);

  }
  if (animY2 == 3){

      phaseY2 = ofxeasing::map(cpt, 0, 80, 0, 2*PI, ofxeasing::sine::easeInOut);

  }
  if (animY3 == 3){

      phaseY3 = ofxeasing::map(cpt, 0, 80, 0, 2*PI, ofxeasing::sine::easeInOut);

  }

  int harmo_size = harmo_w / 4 - 20;

  for (int y = 0; y < n_harmo_y; y++){
    for (int x = 0; x < n_harmo_x; x++){
      int i = y*n_harmo_x + x;
      //      harmos[i].setNumPoints(n_points);
      harmos[i].setOsc(0, 0, x + freqX1 + freqX1_f, phaseX1, harmo_size, dampX1);
      harmos[i].setOsc(0, 1, freqX2 + freqX2_f, phaseX2, harmo_size, dampX2);
      harmos[i].setOsc(1, 0, y + freqY1 + freqY1_f, phaseY1, harmo_size, dampY1);
      harmos[i].setOsc(1, 1, freqY2 + freqY2_f, phaseY2, harmo_size, dampY2);
      harmos[i].setOsc(2, 0, freqX3 + freqX3_f, phaseX3, harmo_size, dampX3);
      harmos[i].setOsc(2, 1, freqY3 + freqY3_f, phaseY3, harmo_size, dampY3);
    }
  }

  // harmos[0].setOsc(0, 0, 1 + freqX1 + freqX1_f, phaseX1, harmo_size, dampX1);
  // harmos[0].setOsc(1, 0, 1 + freqY1 + freqY1_f, phaseY1, harmo_size, dampY1);

  // harmos[1].setOsc(0, 0, 1 + freqX1 + freqX1_f, phaseX1, harmo_size, dampX1);

  // harmos[2].setOsc(0, 0, 1 + freqX1 + freqX1_f, phaseX1, harmo_size, dampX1);
  // harmos[2].setOsc(1, 1, 1 + freqY2 + freqY2_f, phaseY2, harmo_size, dampY2);

  // harmos[4].setOsc(1, 0, 1 + freqY1 + freqY1_f, phaseY1, harmo_size, dampY1);

  // harmos[5].setOsc(1, 1, 1 + freqY2 + freqY2_f, phaseY2, harmo_size, dampY2);

  // harmos[6].setOsc(1, 0, 1 + freqY1 + freqY1_f, phaseY1, harmo_size, dampY1);
  // harmos[6].setOsc(0, 1, 1 + freqX2 + freqX2_f, phaseX2, harmo_size, dampX2);

  // harmos[7].setOsc(0, 1, 1 + freqX2 + freqX2_f, phaseX2, harmo_size, dampX2);

  // harmos[8].setOsc(0, 1, 1 + freqX2 + freqX2_f, phaseX2, harmo_size, dampX2);
  // harmos[8].setOsc(1, 1, 1 + freqY2 + freqY2_f, phaseY2, harmo_size, dampY2);

  for (int i = 0; i < harmos.size(); i++){
    harmos[i].update();
    harmos[i].createTube();
  }

  // harmo.setNumPoints(n_points);
  // harmo.setOsc(0, 0, freqX1 + freqX1_f, phaseX1, 75, dampX1);
  // harmo.setOsc(0, 1, freqX2 + freqX2_f, phaseX2, 75, dampX2);
  // harmo.setOsc(1, 0, freqY1 + freqY1_f, phaseY1, 75, dampY1);
  // harmo.setOsc(1, 1, freqY2 + freqY2_f, phaseY2, 75, dampY2);
  // harmo.setOsc(2, 0, freqX3 + freqX3_f, phaseX3, 75, dampX3);
  // harmo.setOsc(2, 1, freqY3 + freqY3_f, phaseY3, 75, dampY3);




  //  phaseX3 = cpt*2*PI/80;
  
  // if (cpt < 60){
  //   freqX1 = ofxeasing::map(cpt, 0, 60, 1, 2, ofxeasing::linear::easeInOut);
  //   freqY3 = ofxeasing::map(cpt, 0, 60, 0.5, 1.5, ofxeasing::linear::easeInOut);
  // }else{
  //   freqY1 = ofxeasing::map(cpt, 60, 120, 1.5, 0.5, ofxeasing::linear::easeInOut);
  //   freqX1 = ofxeasing::map(cpt, 60, 120, 2, 1, ofxeasing::linear::easeInOut);
  // }
  
  // if (cpt < 40){
  //     n_points = ofxeasing::map(cpt, 0, 40, 2000, 15000, ofxeasing::sine::easeInOut);
  // } else {
  //   n_points = ofxeasing::map(cpt, 40, 80, 15000, 2000, ofxeasing::sine::easeInOut);
  // }
  //cpt = (cpt+1);

  
  // if (cpt < 40){
  //   phaseX1 = ofxeasing::map(cpt%40, 0, 40, 0, 2*PI, ofxeasing::back::easeInOut_s, 0.3);
  // }
  // phaseY1 = ofxeasing::map(cpt%40, 0, 40, 0, 2*PI, ofxeasing::linear::easeInOut);
  cpt = (cpt+1)%80;

  fbo_tex = fbo.getTextureReference();
  
  fbo.begin();	
  ofBackground(0);	
  t = 0;
  step();


  ofEnableDepthTest();
  ofEnableLighting();
  pointLight.enable();
  pointLight2.enable();
  pointLight3.enable();

  
  cam.begin();
  ofPushStyle();
  // shader.begin();
  // shader.setUniformTexture("texture1", fbo_tex, 1);
  // shader.setUniform1f("path_decrease", decrease);
  ofPushMatrix();

  mesh.clear();
  //  ofTranslate(width/2, height/2);
  //ofRotateX(cpt*2.0*PI/80);
  //  mesh.addVertex(ofPoint(x, y, z));
  for (int i = 0; i < n_points; i++){
    float oldX = x;
    float oldY = y;
    float oldZ = z;
    
    //   ofSetColor(255, ofMap(i, 0, n_points, 255, 0));
    mesh.addVertex(ofPoint(mapX(x), mapY(y), mapX(z)));
    step();
    //ofDrawLine(mapY(oldX), mapY(oldY), mapX(oldZ), mapY(x), mapY(y), mapX(z));

    //mesh.addColor(ofColor(255, ofMap(i, 0, n_points, 255, 0)));
    mesh.addColor(ofColor(0, 10+i/300.0, 150+i/300.0));// ofMap(i, 0, n_points, 255, 0)));
    mesh.addIndex(i);
  }
  //  mesh.draw();



  ofPushMatrix();
  ofTranslate(-width/2 + harmo_w/2, -height/2 + harmo_w/2);
  material.begin();
  for (int y = 0; y < n_harmo_y; y++){
    for (int x = 0; x < n_harmo_x;x++){
      //harmos[y*n_harmo_x + x].draw( x*harmo_w, y*harmo_w);
      harmos[y*n_harmo_x + x].draw3D( x*harmo_w, y*harmo_w);
    }
  }
  material.end();
  ofPopMatrix();
  // harmos[1].draw( 300,    0);
  // harmos[2].draw( 600,    0);

  // harmos[3].draw(   0,  300);
  // harmos[4].draw( 300,  300);
  // harmos[5].draw( 600,  300);

  // harmos[6].draw(   0,  600);
  // harmos[7].draw( 300,  600);
  // harmos[8].draw( 600,  600);

  //harmo.draw();
  ofPopMatrix();

  ofPopStyle();
  cam.end();
  fbo_tex.draw(0,0);
  //shader.end();
  fbo.end();
  
  if (b_record){
    if (cpt<80){
      ofPixels pix;
      fbo_tex.readToPixels(pix);
 
      char buffer[256]; 
      sprintf(buffer, "%03d", cpt);
      ofSaveImage(pix, "toto_" + ofToString(buffer) + ".png");
    }
  }


}

//--------------------------------------------------------------
void ofApp::draw(){
  ofBackground(0);

  ofNoFill();
  ofSetColor(255);

  fbo.draw((ofGetWidth()-width)/2,(ofGetHeight()-height)/2);

  ofDisableLighting();

  ofDisableDepthTest();
  if (draw_gui){
    gui.draw();
  }


}



//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  switch(key){
  case 'f':
    ofToggleFullscreen();
    break;
  case 'g':
    draw_gui = !draw_gui;
    break;
  case 'e':
    harmos[0].save("Harmo.ply");
    break;
  case 'r':
    gui.saveToFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case 'l':
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case OF_KEY_RIGHT:
    preset = min(preset + 1, 20);
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case OF_KEY_LEFT:
    preset = max(preset - 1, 0);
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case OF_KEY_UP:
    preset = min(preset + 1, 20);
    break;
  case OF_KEY_DOWN:
    preset = max(preset - 1, 0);
    break;
  case '&':
    preset = 0;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case 233:
    preset = 1;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case '\"':
    preset = 2;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case '\'':
    preset = 3;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case '(':
    preset = 4;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case '-':
    preset = 5;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case 232:
    preset = 6;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case '_':
    preset = 7;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case 231:
    preset = 8;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case 224:
    preset = 9;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//--------------------------------------------------------------
// void ofApp::drawAudio(){
//   if (draw_audio){
//     int posx = ofGetWidth() * 0.1;
//     int posy = ofGetHeight() * 0.9;
//     int sizex = ofGetWidth() * 0.8;
//     int sizey = ofGetHeight() * 0.1;

//     ofPushStyle();
//     ofPushMatrix();

//     ofTranslate(posx, posy, 0);
  
//     ofSetColor(225);
//     ofDrawBitmapString("Left Channel", 4, 18);
//     ofFill();
//     ofSetLineWidth(1);	
//     ofSetColor(255, 255, 255, 150);
//     ofRect(0, 0, sizex, sizey);

//     ofNoFill();  
//     ofSetColor(245, 58, 135);
//     ofSetLineWidth(2);
					
//     ofBeginShape();
//     for (unsigned int i = 0; i < lAudio.size(); i++){
//       float x =  ofMap(i, 0, lAudio.size(), 0, sizex, true);
//       ofVertex(x,  sizey * lAudio[i] + (sizey/2));
//     }
//     ofEndShape(false);
//     ofSetColor(58, 135, 245);
//     ofBeginShape();
//     for (unsigned int i = 0; i < rAudio.size(); i++){
//       float x =  ofMap(i, 0, rAudio.size(), 0, sizex, true);
//       ofVertex(x, rAudio[i] * sizey + (sizey/2));
//     }
//     ofEndShape(false);

//     ofPopMatrix();
//     ofPopStyle();
//   }
// }

// //--------------------------------------------------------------
// void ofApp::audioOut(float * output, int bufferSize, int nChannels){
//   if (mode_audio){
    
//     //float correction = ofMap(ofGetLastFrameTime(), 0, 0.0167, 0, 1);
    
//     // for (int i = 0; i < 512; ++i){
//     // 	o_s.rotate(v, ofVec3f(0,0,1)); 
//     // 	p1_s.rotate(v1, ofVec3f(0,0,1)); 
//     // 	p2_s.rotate(v2, ofVec3f(0,0,1)); 
	
//     // 	ofPoint pos1_s = c1_s.getGlobalPosition();
//     // 	ofPoint pos2_s = c2_s.getGlobalPosition();
	
//     // 	int_pos_s = pos1_s + arm_x * (pos2_s - pos1_s).normalize();
	
//     // 	ofPoint new_pos = int_pos_s +  arm_y * (pos2_s - pos1_s).getPerpendicular(ofVec3f(0,0,1));

//     // 	output[i*nChannels    ] = lAudio[i] = output[i*nChannels +1 ] = rAudio[i] = ofMap(new_pos.x, 0.0, 1920, -0.5, 0.5, true) + ofMap(new_pos.y, ll, rr, -0.5, 0.5, true);
	
//     //   }
//   }
// }
