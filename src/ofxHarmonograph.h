#ifndef HARMONOGRAPH_H
#define HARMONOGRAPH_H

#pragma once

#include "ofMain.h"

#include "oscillator.h"

class Harmonograph
{
 public:
  Harmonograph();
  virtual ~Harmonograph();
  
  void update();
  void draw();
  void draw(int pos_x, int pos_y);
  void draw3D(int pos_x, int pos_y);

  void save(string filename);
  ofPoint step(float t);
  void setNumPoints(int n_points);
  void setOsc(int chan, int num, float freq, float phase, float ampli, float damp);

  void createRibbon();
  void createTube();
  
 protected:
  int n_points;
  ofVboMesh mesh;
  ofVboMesh mesh3d;
  float DT;
  
  float radius;
  int n_radius;

  std::vector<Oscillator> osc_x;
  std::vector<Oscillator> osc_y;
  std::vector<Oscillator> osc_z;
};

#endif
